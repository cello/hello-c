;; -*- lisp-version: "7.0 [Windows] (Jun 10, 2005 13:34)"; cg: "1.54.2.17"; -*-

(in-package :cg-user)

(defpackage :HELLO-C)

(define-project :name :hello-c
  :modules (list (make-instance 'module :name "package.lisp")
                 (make-instance 'module :name "primitives.lisp")
                 (make-instance 'module :name "objects.lisp")
                 (make-instance 'module :name "aggregates.lisp")
                 (make-instance 'module :name "functions.lisp")
                 (make-instance 'module :name "strings.lisp")
                 (make-instance 'module :name "libraries.lisp")
                 (make-instance 'module :name "os.lisp")
                 (make-instance 'module :name "definers.lisp")
                 (make-instance 'module :name "callbacks.lisp")
                 (make-instance 'module :name "arrays.lisp"))
  :projects nil
  :libraries nil
  :distributed-files nil
  :internally-loaded-files nil
  :project-package-name :hello-c
  :main-form nil
  :compilation-unit t
  :verbose nil
  :runtime-modules nil
  :splash-file-module (make-instance 'build-module :name "")
  :icon-file-module (make-instance 'build-module :name "")
  :include-flags '(:local-name-info)
  :build-flags '(:allow-debug :purify)
  :autoload-warning t
  :full-recompile-for-runtime-conditionalizations nil
  :default-command-line-arguments "+cx +t \"Initializing\""
  :additional-build-lisp-image-arguments '(:read-init-files nil)
  :old-space-size 256000
  :new-space-size 6144
  :runtime-build-option :standard
  :on-initialization 'default-init-function
  :on-restart 'do-default-restart)

;; End of Project Definition
