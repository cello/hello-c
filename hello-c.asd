;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;(declaim (optimize (debug 2) (speed 1) (safety 1) (compilation-speed 1)))
(declaim (optimize (debug 3) (speed 3) (safety 1) (compilation-speed 0)))


#+(or allegro lispworks cmu mcl cormanlisp sbcl scl)

(asdf:defsystem :hello-c
  :name "Hello C"
  :author "Kenny Tilton <ktilton@nyc.rr.com>"
  :version "1.0.0"
  :maintainer "Kenny Tilton <ktilton@nyc.rr.com>"
  :licence "Lisp Lesser GNU Public License"
  :description "Portable C Interface Tools"
  :long-description "Portable Tools for Using C from Lisp, largely forked from UFFI"
  :components ((:file "package")
               (:file "primitives" :depends-on ("package"))
               #+mcl (:file "readmacros-mcl" :depends-on ("package"))
               (:file "objects" :depends-on ("primitives"))
               (:file "strings" :depends-on ("primitives" "functions" "aggregates" "objects"))
               (:file "aggregates" :depends-on ("primitives"))
               (:file "functions" :depends-on ("primitives"))
               (:file "libraries" :depends-on ("package"))
               (:file "os" :depends-on ("package"))
               (:file "definers" :depends-on ("package"))
               (:file "arrays" :depends-on ("package"))
               (:file "callbacks" :depends-on ("package"))))
